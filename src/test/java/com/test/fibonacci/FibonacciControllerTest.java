package com.test.fibonacci;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.*;

class FibonacciControllerTest {
    FibonacciController controller = new FibonacciController();

    @Test
    void calculate() throws InterruptedException {

        Assertions.assertEquals(7,controller.Calculate(4));
    }
    
    @Test
    void calculate_InvalidInput() throws InterruptedException {
        Exception exception = assertThrows(
                ResponseStatusException.class,
                () -> controller.Calculate(-4));

       assertEquals("400 BAD_REQUEST \"Provide whole number\"", exception.getMessage());

       
    }

    @Test
    void calculate_OverflowException() throws InterruptedException {
        int k = Integer.MAX_VALUE-2;
        Exception exception = assertThrows(
                ArithmeticException.class,
                () -> controller.Calculate(k));

        assertEquals("integer overflow", exception.getMessage());


    }




    @Test
    void testFibonacci(){
        Assertions.assertEquals(0,controller.fibonacci(0));
        Assertions.assertEquals(1,controller.fibonacci(1));
        Assertions.assertEquals(13,controller.fibonacci(7));
        Assertions.assertEquals(6765,controller.fibonacci(20));


    }
}