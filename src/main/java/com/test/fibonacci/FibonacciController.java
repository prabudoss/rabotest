package com.test.fibonacci;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@RestController
public class FibonacciController {

    @GetMapping(path = "/calculate/{number}")
    public int Calculate(@PathVariable int number) throws InterruptedException {

        if(number<0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Provide whole number");
        }

        int sum =0;

        for(int i=0;i<=number;i++){
            int j = fibonacci(i);
            TimeUnit.SECONDS.sleep((long) getTimeDelay());
            System.out.println(" fibonacci(i)" + i +"is " +j);
            sum = Math.addExact(sum, j);
        }
        return sum;

    }


    private float getTimeDelay(){
        Random random = new Random();
        return random.nextFloat();

    }

    public int fibonacci(int number){
        int k = Stream.iterate(new int[]{0, 1}, s -> new int[]{s[1], s[0] + s[1]})
                .limit(number+1)
                .skip(number)
                .findFirst()
                .get()[0];
        return k;
    }

}
